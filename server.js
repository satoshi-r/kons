const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const {readFileSync, writeFileSync} = require("fs");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'dist')));


// настройка CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, PATCH, PUT, POST, DELETE, OPTIONS");
  next();  // передаем обработку запроса методу app.post("/request-call"...
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});




app.post('/api/request-call', (req, res) => {
  try {
    const json = readFileSync('calls.json', 'utf8');
    const parsedJson = JSON.parse(json);
    parsedJson.push(req.body);
    writeFileSync('calls.json', JSON.stringify(parsedJson));
  } catch (err) {
    const data = [req.body];
    writeFileSync('calls.json', JSON.stringify(data));
  }

  res.status(200).send("Wait for a call");
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));
