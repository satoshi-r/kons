// импорт модулей
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {HttpClientModule} from "@angular/common/http";

// импорт компонентов
import {AppComponent} from './app.component';
import {HeaderComponent} from './shared/header/header.component';
import {HomeComponent} from './shared/home/home.component';
import {ContactsComponent} from './shared/contacts/contacts.component';
import {PopupComponent} from './popup/popup.component';
import {FormComponent} from './form/form.component';

// пути для страниц
const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contacts', component: ContactsComponent},
]

// регистрация модулей и компонентов
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ContactsComponent,
    PopupComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
