import {Component, OnInit, Injectable} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

@Injectable()
export class FormComponent implements OnInit {

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  public inputTel: string = ""; // поле ввода телефона
  public message: string = ""; // уведомление об отправке
  public formSubmitted: boolean = false;

  // валидация поля телефона
  telFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(5),
    Validators.pattern(/^[\d.,()+-]+$/)
  ]);

  /**
   * обработчик события submit формы
   * @param evt
   */
  onSubmited(evt: any): void {
    const date = new Date();
    const time: string = `${date.getHours()}:${date.getMinutes()}`
    this.formSubmitted = true;

    /**
     * отправить телефон и время отправки на сервер и после отправки
     * в переменную message вывести ответ сервера
     */
    this.http.post("http://localhost:3000/api/request-call",
      {tel: this.inputTel, time},
      {responseType: "text"})
      .subscribe((data: any) => this.message = data);
  }
}
